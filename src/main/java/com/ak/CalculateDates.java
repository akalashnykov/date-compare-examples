package com.ak;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by admin on 8/21/2015.
 */
public class CalculateDates {

    static final Logger logger = (Logger) LoggerFactory
            .getLogger(CalculateDates.class);

    public static void isBusinessDay(Date dateToCheck, HashSet holidays) {

        logger.info("today: " + dateToCheck.toString());

        Calendar baseCal1 = Calendar.getInstance();
        logger.info("Calendar 1 (default locale): " + baseCal1.toString());

        Calendar baseCal2 = Calendar.getInstance(Locale.US);
        logger.info("Calendar 2 (US locale): " + baseCal2.toString());

    }

    public void testCalendars() throws Exception {
        logger.info("Calendar test");
        logger.info("java vesion: " + System.getProperty("java.version"));

        isBusinessDay(new Date(), null);

        class MyDateComparator implements Comparator<Date> {
            protected final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

            public int compare(Date d1, Date d2) {
                logger.info("comparing dates - d1: " + d1 + ", d2: " + d2);
                return DATE_FORMAT.format(d1).compareTo(DATE_FORMAT.format(d2));
            }
        }

        // Old method

        Date dateToCheck = new Date(); // today
        Date dateToCheckTruncated = DateUtils.truncate(dateToCheck, Calendar.DATE); // remove hh:mm:ss
        logger.info("today: " + dateToCheck);
        logger.info("today truncated: " + dateToCheckTruncated);

        Calendar calendar = Calendar.getInstance(Locale.US);
        calendar.setTime(dateToCheckTruncated);
        logger.info("calendar: " + calendar.toString());

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        boolean onWeekend = dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;

        if (onWeekend) {
            logger.info("today is Weekend day: ");
        } else {
            logger.info("today is Not a weekend day");
        }

        logger.info("today in millis: " + dateToCheck.getTime());
        logger.info("today truncated in millis: " + dateToCheckTruncated.getTime());
        logger.info("calendar in millis: " + calendar.getTimeInMillis());

        long todayInMillis = calendar.getTimeInMillis();

        List<Date> offlimitDates = new ArrayList<Date>();

        offlimitDates.add((calendar.getTime()));
        calendar.add(Calendar.DAY_OF_MONTH, -40);
        offlimitDates.add((calendar.getTime()));
        calendar.add(Calendar.DAY_OF_MONTH, +50);
        offlimitDates.add((calendar.getTime()));

        Collections.sort(offlimitDates);
        logger.info("offlimitDates: " + offlimitDates);

        calendar.setTime(dateToCheckTruncated);
        if (offlimitDates.contains(calendar.getTime())) {
            logger.info("Old method: today is Holiday");
        } else {
            logger.info("Old method: today in Business Day");
        }


        // new method 1
        Collections.sort(offlimitDates);
        Set<Long> offLimitsLong = new HashSet<Long>();

        for (Date dt : offlimitDates) {
            logger.info("offlimitDate: " + dt.toString() + "offlimitDateLong: " + dt.getTime());
            offLimitsLong.add(dt.getTime()); // get long
        }

        logger.info("offLimitsLong: " + offLimitsLong);

        calendar.setTime(dateToCheckTruncated);
        if (offLimitsLong.contains(calendar.getTimeInMillis())) {
            logger.info("New method1: today is Holiday");
        } else {
            logger.info("New method1: today in Business Day");
        }

        // new method 2

        int index = Collections.binarySearch(offlimitDates, calendar.getTime(), new MyDateComparator());

        if (index >= 0) {
            logger.info("New method2: today is Holiday");
        } else {
            logger.info("New method2: today in Business Day");
        }
    }

    public static void main(String[] args) throws Exception {
        CalculateDates cd = new CalculateDates();

        cd.testCalendars();

    }

}
